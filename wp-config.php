<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'hpe' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'i5q1: <0]h`x^<} 01Ic}`nq;PqFrcCi[`:xO1NR+#&kB|E4$G:/(3S<[SWju<id' );
define( 'SECURE_AUTH_KEY',  'x7g1FeS[)}jiEJEzB`^6FEzn.wx1gJtJLu,{1>0kG=QK_.zg,8aM^~e|Xz-GlO_v' );
define( 'LOGGED_IN_KEY',    '. Rd;`?wDJB/C0ZdA k?8N2|lNgl$hz1s-&.)n13ZHL~( l0[B,7bVL)s,?sG25`' );
define( 'NONCE_KEY',        '4ow*Ug2n;Qq3,ajPaMmZ~SxE.7;}[A##R%t>Q;w*7>>+-]34<7z6U*WElPhu$9B0' );
define( 'AUTH_SALT',        '/%oLV2?O.Bewa}-a>H,.J/ia2Tn94nZ1^f6xrR$CU6ah{WG#wvLH/M&$;JC*%G+.' );
define( 'SECURE_AUTH_SALT', '[Bk0Ho),]c8rCe&?sxpj|9~9Nuk|91j8t{k[oL,B$JW/&h<3#HM(ehN}wNIluthb' );
define( 'LOGGED_IN_SALT',   '|0-F;wkWO/l6CFM)/o-6=0CGPESihiuvY6;)v+,z4Cd <_d]{#s;p2~@su]~m<SP' );
define( 'NONCE_SALT',       'IDHEK-bFtSN=~D<Qbh?0BBo Y)W|PK,ZiTD9]4c5xCY6Z!k%jrEmdBO!<g0HuQ?2' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
