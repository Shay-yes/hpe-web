<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HPE
 */

?>
	<div class="dynamic-footnotes dynamicFootnotesComponent parbase"></div>

	<footer class="hpe-footer hf-v2" role="contentinfo">
	    <div class="footer-iparsys iparsys parsys">
	        <div class="newpar new section"> </div>
	        <div class="par iparys_inherited">
	            <div class="globalFooterNavigationComponent parbase">
	                <div id="hf-footer-v3" class="parallax " core-view="">
	                	<div class="hf-centered-content">
	                		<div class="row">
	                			<div class="col-md-8">
				              		<div class="hf-footer-logo">
				              			<a class="hf-logo-btn" title="Hpe" aria-label="Hewlett Packard Enterprise" href="<?php echo get_home_url(); ?>">
		                                                 <img src="<?php echo get_home_url(); ?>/wp-content/themes/hpe/assets/images/logo-2.png">
		                                </a>
				              		</div>
			                        <div class="hf-footer-infor">
			                        	<h2 class="name">Công Ty Cổ Phần Công Nghệ Elite</h2>
			                        	<p class="address">Địa chỉ: 289/1 Ung Văn Khiêm, Phường 25, Quận Bình Thạnh, Thành phố Hồ Chí Minh</p>
			                        	<div class="list-infor">
									      <div class="phone"><i class="fas fa-phone-alt"></i>&nbsp;&nbsp;(84-28) 3512 3959 | </div>
									      <div class="mail"><i class="fas fa-envelope"></i>&nbsp;&nbsp;kai@elite-jsc.com | </div>
									      <div class="web"><a href="elite-jsc.com" target="_blank" rel="noopener noreferrer"><i class="fas fa-globe-americas"></i>&nbsp;&nbsp;elite-jsc.com</a></div>
									    </div>
			                        </div>
	                        	</div>
	                        	<div class="col-md-4">
	                        		<div class="hf-footer-menu">
	                        			<h4 class="title-menu">SITE MAP</h4>
											<div><a href="/san-pham/">Sản phẩm</a></div>
											<div><a href="/giai-phap/">Giải pháp</a></div>
											<div><a href="/tin-tuc/">Tin tức</a></div>
											<div><a href="/lien-he/">Liên hệ</a></div>
											
	                        		</div>
	                        	</div>
		                        <div class="col-md-12" >
		                            <div class="hf-col-flex">
		                                <div id="hf-copyright-v3"> © Copyright 2020 . All Rights Reserved. </div>
		                            </div>
		                        </div>
	                    	</div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</footer>

    <div id="overlay-loader" class="overlay-validation"> <button class="spinnerContainer">Loading information, please wait</button> </div>
        <div class="hpe-overlay modal" tabindex="-1" role="dialog" aria-labelledby="HPEModal">
            <div class="container-fluid container-gutter">
                <div class="row hpe-overlay-container style-bg-white">
                    <div class="col-xs-12 overlay-content"> </div>
                    <a href="#" class="icon-link icon-nav-close-menu btn-close-form" data-dismiss="modal">Close Overlay</a> <a href="#" class="icon-link icon-arrow-pointer-left btn-collapse-form" data-dismiss="modal">Collapse Overlay - Back Link</a> 
                    <div id="hpe-overlay-loader" class="overlay-validation fade"> <button class="spinnerContainer">Loading information, please wait</button> </div>
                </div>
            </div>
        </div>
        
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/apps/hpeweb-ui/js/hpe.dll.libs.js"></script> 
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/apps/hpeweb-ui/js/hpe.hpe-generic-v3.e308743f30070f365556.js"></script> 
	</div>
</div><!-- #page -->

<script>
	$(document).ready(function() {
		$('.carousel-item').click(function() {
			let id = $(this).attr('data-id');

			$('.carousel-item').removeClass('active');
			$(this).addClass('active');

			$('.carousel-content').removeClass('active');
			$('.tab-' + id).addClass('active');

		});
	});
</script>

<?php wp_footer(); ?>

</body>
</html>
