<?php
/**
* Template Name: HPE - Contact
*
* @package WordPress
* @subpackage HPE
* @since HPE 1.0
*/

get_header();
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/elite.min.css">
<main id="main" class="clearfix width-100">
    <div class="fusion-row" style="max-width:100%;">
        <section id="content" class="full-width">
            <div id="post-16" class="post-16 page type-page status-publish hentry">			
                <div class="post-content">
                    <div class="fusion-fullwidth fullwidth-box fusion-builder-row-1 fusion-parallax-none nonhundred-percent-fullwidth non-hundred-percent-height-scrolling lazyloaded" style="background-color: rgba(255, 255, 255, 0); background-position: center center; background-repeat: no-repeat; padding: 0px; background-size: cover; background-image: url(&quot;http://demo.vgroup.vn/wp-content/uploads/2020/07/bgr-trouble-1.jpg&quot;);" data-bg="http://demo.vgroup.vn/wp-content/uploads/2020/07/bgr-trouble-1.jpg">
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1 fusion-builder-column-0 fusion-one-full fusion-column-first fusion-column-last page-contact-form-bgr 1_1" style="margin-top:0px;margin-bottom:0px;">
                                <div class="fusion-column-wrapper" style="background-color: rgb(0, 0, 0); padding: 50px 0px; background-position: left top; background-repeat: no-repeat; background-size: cover; height: auto;" data-bg-url="">
                                    <div class="fusion-builder-row fusion-builder-row-inner fusion-row ">
                                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6 fusion-builder-nested-column-1 fusion-one-sixth fusion-column-first 1_6" style="margin-top: 0px;margin-bottom: 20px;width:16.66%;width:calc(16.66% - ( ( 4% + 4% ) * 0.1666 ) );margin-right:4%;">
                                            <div class="fusion-column-wrapper fusion-column-wrapper-1" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                            </div>
                                        </div>
                                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_2_3 fusion-builder-nested-column-2 fusion-two-third 2_3" style="margin-top: 0px;margin-bottom: 20px;width:66.66%;width:calc(66.66% - ( ( 4% + 4% ) * 0.6666 ) );margin-right:4%;">
                                            <div class="fusion-column-wrapper fusion-column-wrapper-2" style="padding: 0px; background-position: left top; background-repeat: no-repeat; background-size: cover; height: auto;" data-bg-url="">
                                                <div role="form" class="wpcf7" id="wpcf7-f431-p16-o1" lang="en-US" dir="ltr">
                                                    <div class="screen-reader-response" role="alert" aria-live="polite"></div>
                                                    <form class="wpcf7-form init" novalidate="novalidate">
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Họ và tên</div>
                                                            <div><span class="wpcf7-form-control-wrap text-475"><input type="text" name="text-475" value="" size="40" class="wpcf7-form-control wpcf7-text wctf7-input" aria-invalid="false" placeholder="Họ và tên"></span></div>
                                                        </div>
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Tên công ty</div>
                                                            <div><span class="wpcf7-form-control-wrap tencongty"><input type="text" name="tencongty" value="" size="40" class="wpcf7-form-control wpcf7-text wctf7-input" aria-invalid="false" placeholder="Nhập tên công ty"></span></div>
                                                        </div>
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Chức danh/bộ phận</div>
                                                            <div><span class="wpcf7-form-control-wrap chucdanh"><input type="text" name="chucdanh" value="" size="40" class="wpcf7-form-control wpcf7-text wctf7-input" aria-invalid="false" placeholder="Chức danh/bộ phận"></span></div>
                                                        </div>
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Số điện thoại</div>
                                                            <div><span class="wpcf7-form-control-wrap sodienthoai"><input type="text" name="sodienthoai" value="" size="40" class="wpcf7-form-control wpcf7-text wctf7-input" aria-invalid="false" placeholder="Nhập số điện thoại"></span></div>
                                                        </div>
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Địa chỉ email</div>
                                                            <div><span class="wpcf7-form-control-wrap email-230"><input type="email" name="email-230" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email wctf7-input" aria-invalid="false" placeholder="Nhập email"></span></div>
                                                        </div>
                                                        <div style="margin: 30px 0px; width:100%">
                                                            <div style="margin-bottom: 15px; color:#fff">Lời nhắn</div>
                                                            <div><span class="wpcf7-form-control-wrap textarea-824"><textarea name="textarea-824" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wctf7-input" aria-invalid="false" placeholder="Nội dung cần tư vấn"></textarea></span></div>
                                                        </div>
                                                        <div style="text-align: center">
                                                            <input type="submit" value="Gửi" class="wpcf7-form-control wpcf7-submit wctf7-submit-contact">
                                                            <div class="fusion-slider-loading" style="display: none;"></div>
                                                        </div>
                                                        <div class="fusion-alert alert custom alert-custom fusion-alert-center wpcf7-response-output alert-dismissable" style="border-width:1px;">
                                                            <button type="button" class="close toggle-alert" data-dismiss="alert" aria-hidden="true">×</button>
                                                            <div class="fusion-alert-content-wrapper"><span class="fusion-alert-content"></span></div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_6 fusion-builder-nested-column-3 fusion-one-sixth fusion-column-last 1_6" style="margin-top: 0px;margin-bottom: 20px;width:16.66%;width:calc(16.66% - ( ( 4% + 4% ) * 0.1666 ) );">
                                            <div class="fusion-column-wrapper fusion-column-wrapper-3" style="padding: 0px 0px 0px 0px;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;" data-bg-url="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">.fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link) , .fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):before, .fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):after {color: #212934;}.fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover, .fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover:before, .fusion-fullwidth.fusion-builder-row-1 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover:after {color: #01a982;}.fusion-fullwidth.fusion-builder-row-1 .pagination a.inactive:hover, .fusion-fullwidth.fusion-builder-row-1 .fusion-filters .fusion-filter.fusion-active a {border-color: #01a982;}.fusion-fullwidth.fusion-builder-row-1 .pagination .current {border-color: #01a982; background-color: #01a982;}.fusion-fullwidth.fusion-builder-row-1 .fusion-filters .fusion-filter.fusion-active a, .fusion-fullwidth.fusion-builder-row-1 .fusion-date-and-formats .fusion-format-box, .fusion-fullwidth.fusion-builder-row-1 .fusion-popover, .fusion-fullwidth.fusion-builder-row-1 .tooltip-shortcode {color: #01a982;}#main .fusion-fullwidth.fusion-builder-row-1 .post .blog-shortcode-post-title a:hover {color: #01a982;}</style>
                    <div class="fusion-fullwidth fullwidth-box fusion-builder-row-2 fusion-parallax-none page-contact-introduce-bgr nonhundred-percent-fullwidth non-hundred-percent-height-scrolling lazyloaded" style="background-color: rgba(255, 255, 255, 0); background-position: center top; background-repeat: no-repeat; padding: 0px; margin-top: 0px; background-size: cover; background-image: url(&quot;http://demo.vgroup.vn/wp-content/uploads/2020/07/Subtract-1.png&quot;);" data-bg="http://demo.vgroup.vn/wp-content/uploads/2020/07/Subtract-1.png">
                        <div class="fusion-builder-row fusion-row ">
                            <div class="fusion-layout-column fusion_builder_column fusion_builder_column_1_1 fusion-builder-column-1 fusion-one-full fusion-column-first fusion-column-last 1_1" style="margin-top:0px;margin-bottom:20px;">
                                <div class="fusion-column-wrapper" style="background-color: rgba(0, 0, 0, 0); padding: 0px; background-position: left top; background-repeat: no-repeat; background-size: cover; height: auto;" data-bg-url="">
                                    <div class="fusion-text">
                                        <div class="big-title">
                                            <h2 data-fontsize="34" data-lineheight="40.8px" class="fusion-responsive-typography-calculated" style="--fontSize:34; line-height: 1.2;">Giới thiệu chung về Công ty Cổ Phần Công Nghệ Elite</h2>
                                        </div>
                                    </div>
                                    <div class="fusion-text g-color-white">
                                        <p style="text-align: left;">Được thành lập năm 1995 bởi bà Trần Hoài Phương Chi, Công ty cổ phần Elite (tiền thân là Công ty TNHH Win Technology) hiện đang nằm trong top 3 nhà phân phối công nghệ hàng đầu tại Việt Nam với các đối tác lớn như HPI, HPE, Microsoft, Oracle, VMWare, Fujtsu, Huawei… Với phương châm “Luôn là nhà phân phối chân chính – “True Distributor”, Công ty Cổ phần Công nghệ Elite được biết đến như là một đối tác tin cậy trong lĩnh vực cung ứng giải pháp trọn gói phần cứng, phần mềm, bảo hành uy tín và hiệu quả trong các dự án CNTT của chính phủ, giáo dục, ngân hàng… và tất cả doanh nghiệp lớn, vừa và nhỏ.</p>
                                        <p>Trong 25 năm đổi mới và phát triển không ngừng, phương châm “Nhân lực là tài sản quý giá của công ty” là kim chỉ nan cho Ban lãnh đạo. Với môi trường làm việc học hỏi, hỗ trợ và chuyên nghiệp cùng với các chế độ đãi ngộ xứng đáng cho hơn 250 nhân viên, Elite sẽ không ngừng cập nhật, đổi mới để bắt kịp xu hướng công nghệ thông tin hiện đại nhất, với mục đích là tối đa hóa lợi ích cho khách hàng và đóng góp cho sự phát triển của kinh tế nước nhà.</p>
                                    </div>
                                    <div class="imageframe-align-center"><span class="fusion-imageframe imageframe-none imageframe-1 hover-type-none"><img src="http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1.png" data-orig-src="http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1.png" width="753" height="425" alt="" title="Rectangle 46 (4)" class="img-responsive wp-image-446 lazyautosizes lazyloaded" srcset="http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-200x113.png 200w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-400x226.png 400w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-600x339.png 600w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1.png 753w" data-srcset="http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-200x113.png 200w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-400x226.png 400w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1-600x339.png 600w, http://smb-server.elite-jsc.vn/wp-content/uploads/2020/07/Rectangle-46-4-1.png 753w" data-sizes="auto" data-orig-sizes="(max-width: 800px) 100vw, 753px" sizes="753px"></span></div>
                                    <div style="font-size: 18px;line-height: 25px; margin: 50px 0px; border-top: 1px solid #fff; padding-top: 50px; color: #fff; display: flex; justify-content: space-between; flex-wrap: wrap">
                                        <div style="display: none; ">
                                            <div style="font-size: 18px;line-height: 25px">Công Ty Cổ Phần Công Nghệ Elite</div>
                                            <div style="font-size: 18px;line-height: 25px">Địa chỉ: 289/1 Ung Văn Khiêm, Phường 25, Quận Bình Thạnh, Thành phố Hồ Chí Minh</div>
                                            <div style="line-height: 25px">
                                                <div style="display: inline-block; font-size:12px ;"><i class="fas fa-phone-alt"></i> : (84-28) 3512 3959 |</div>
                                                <div style="display: inline-block; font-size:12px ;"><i class="fas fa-envelope"></i>: kai@elite-jsc.com |</div>
                                                <div style="display: inline-block; font-size:12px ;"><i class="fas fa-globe-americas"></i> : elite-jsc.com</div>
                                            </div>
                                        </div>
                                        <div style="display: none; ">
                                            <div style="font-size: 18px;line-height: 21px">Kết nối với Elite JSC</div>
                                            <div style="display:flex ; justify-content: end; margin-top:15px">
                                                <div style="background:#616161; height:35px; width:35px !important; position:relative; margin-right:10px; border-radius: 50%;">
                                                    <i style="font-size:12px ;position:absolute; left:50%; top:50%; transform:translate(-50%,-50%);" class="fab fa-facebook-f"></i>
                                                </div>
                                                <div style="background:#616161; height:35px; width:35px !important; position:relative; margin-right:10px; border-radius: 50%;">
                                                    <i style="font-size:12px ;position:absolute; left:50%; top:50%; transform:translate(-50%,-50%);" class="fab fa-twitter"></i>
                                                </div>
                                                <div style="background:#616161; height:35px; width:35px !important; position:relative; margin-right:10px; border-radius: 50%;">
                                                    <div style="font-size:12px ;position:absolute; left:50%; top:50%; transform:translate(-50%,-50%);">Zalo</div>
                                                </div>
                                                <div style="background:#616161; height:35px; width:35px !important; position:relative; border-radius: 50%;">
                                                    <i style="font-size:12px ;position:absolute; left:50%; top:50%; transform:translate(-50%,-50%);" class="fab fa-instagram"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="fusion-clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style type="text/css">.fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link) , .fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):before, .fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):after {color: #212934;}.fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover, .fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover:before, .fusion-fullwidth.fusion-builder-row-2 a:not(.fusion-button):not(.fusion-builder-module-control):not(.fusion-social-network-icon):not(.fb-icon-element):not(.fusion-countdown-link):not(.fusion-rollover-link):not(.fusion-rollover-gallery):not(.fusion-button-bar):not(.add_to_cart_button):not(.show_details_button):not(.product_type_external):not(.fusion-quick-view):not(.fusion-rollover-title-link):not(.fusion-breadcrumb-link):hover:after {color: #01a982;}.fusion-fullwidth.fusion-builder-row-2 .pagination a.inactive:hover, .fusion-fullwidth.fusion-builder-row-2 .fusion-filters .fusion-filter.fusion-active a {border-color: #01a982;}.fusion-fullwidth.fusion-builder-row-2 .pagination .current {border-color: #01a982; background-color: #01a982;}.fusion-fullwidth.fusion-builder-row-2 .fusion-filters .fusion-filter.fusion-active a, .fusion-fullwidth.fusion-builder-row-2 .fusion-date-and-formats .fusion-format-box, .fusion-fullwidth.fusion-builder-row-2 .fusion-popover, .fusion-fullwidth.fusion-builder-row-2 .tooltip-shortcode {color: #01a982;}#main .fusion-fullwidth.fusion-builder-row-2 .post .blog-shortcode-post-title a:hover {color: #01a982;}</style>
                </div>
            </div>
        </section>
    </div>
    <!-- fusion-row -->
</main>

<?php 
get_footer();
?>