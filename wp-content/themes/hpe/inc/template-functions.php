<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package HPE
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function hpe_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'hpe_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function hpe_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'hpe_pingback_header' );

// Control core classes for avoid errors
if ( class_exists( 'CSF' ) ) {

	// Set a unique slug-like ID
	$prefix = 'devng_option';
	
  	// Create options
  	CSF::createOptions( $prefix, array(
    	'menu_title' => 'Cài đặt nội dung',
    	'menu_slug'  => 'content-management',
  	) );

  	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Trang chủ',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-home-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	      	array(
	        	'id'    => 'opt-home-banner-h1',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề H1',
	     	),
	     	array(
	        	'id'    => 'opt-home-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Nội dung sau H1',
	     	),
	     	array(
			  	'id'        => 'opt-home-button',
			  	'type'      => 'fieldset',
			  	'title'     => 'Banner - Button',
			  	'fields'    => array(
				    array(
				      	'id'    => 'opt-button-href',
				      	'type'  => 'text',
				      	'title' => 'Liên kết',
				    ),
				    array(
				      	'id'    => 'opt-button-text',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề Button',
				    ),
			  	),
			),

	     	// Section TRANSFORM TO DIGITAL MANUFACTURING
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: TRANSFORM TO DIGITAL MANUFACTURING',
			),
			array(
	        	'id'    => 'opt-home-section2-h1',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề',
	     	),
	     	array(
	        	'id'    => 'opt-home-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section DIGITAL MANUFACTURING SOLUTIONS
	     	array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: DIGITAL MANUFACTURING SOLUTIONS',
			),
			array(
	        	'id'    => 'opt-home-section3-h1',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề',
	     	),
	     	array(
	        	'id'    => 'opt-home-section3-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),
	     	array(
			  	'id'        => 'opt-home-section3-group',
			  	'type'      => 'group',
			  	'title'     => 'Dữ liệu Carousel Slide',
			  	'fields'    => array(
			    	array(
					  	'id'     => 'opt-home-section3-carousel',
					  	'type'   => 'fieldset',
					  	'title'  => 'Carousel Item',
					  	'fields' => array(
						    array(
						      	'id'    => 'opt-image',
						      	'type'  => 'media',
						      	'title' => 'Hình ảnh',
						    ),
						    array(
						      	'id'    => 'opt-label',
						      	'type'  => 'text',
						      	'title' => 'Label',
						    ),
						    array(
						      	'id'    => 'opt-title',
						      	'type'  => 'text',
						      	'title' => 'Tiêu đề',
						    ),
						    array(
						      	'id'    => 'opt-des',
						      	'type'  => 'textarea',
						      	'title' => 'Mô tả',
						    ),
						    array(
							  	'id'        => 'opt-link',
							  	'type'      => 'fieldset',
							  	'title'     => 'Button',
							  	'fields'    => array(
								    array(
								      	'id'    => 'opt-href',
								      	'type'  => 'text',
								      	'title' => 'Liên kết',
								    ),
								    array(
								      	'id'    => 'opt-title',
								      	'type'  => 'text',
								      	'title' => 'Tiêu đề Button',
								    ),
							  	),
							),
					  	),
					),
			  	),
			),

			// Section SUCCESS IN ACTION
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: SUCCESS IN ACTION',
			),
			array(
	        	'id'    => 'opt-home-section4-success-in-action',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề Section SUCCESS IN ACTION',
	     	),
	     	array(
	        	'id'    => 'opt-home-section4-logo',
	        	'type'  => 'media',
	        	'title' => 'Logo Section SUCCESS IN ACTION',
	     	),
	     	array(
			  	'id'     => 'opt-home-section4-fieldset-left',
			  	'type'   => 'fieldset',
			  	'title'  => 'Dữ liệu Tab trái',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-h1',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề H1',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới H1',
				    ),
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-text',
					  	'type'      => 'group',
					  	'title'     => 'Mô tả dưới ảnh',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề',
					    	),
						    array(
						      	'id'    => 'opt-text',
						      	'type'  => 'textarea',
						      	'title' => 'Nội dung',
						    )
					  	),
					),
					array(
					  	'id'        => 'opt-group-quote',
					  	'type'      => 'group',
					  	'title'     => 'Văn bản trích dẫn',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-text',
					      		'type'  => 'textarea',
					      		'title' => 'Nội dung',
					    	),
						    array(
						      	'id'    => 'opt-source',
						      	'type'  => 'text',
						      	'title' => 'Nguồn nội dung',
						    )
					  	),
					),
					array(
					  	'id'        => 'opt-group-button',
					  	'type'      => 'group',
					  	'title'     => 'Button',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-icon',
					      		'type'  => 'media',
					      		'title' => 'Icon',
					    	),
						    array(
						      	'id'    => 'opt-text',
						      	'type'  => 'text',
						      	'title' => 'Tiêu đề',
						    ),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-home-section4-fieldset-right',
			  	'type'   => 'fieldset',
			  	'title'  => 'Dữ liệu Tab phải',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-h1',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề H1',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới H1',
				    ),
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-text',
					  	'type'      => 'group',
					  	'title'     => 'Mô tả dưới ảnh',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề',
					    	),
						    array(
						      	'id'    => 'opt-text',
						      	'type'  => 'textarea',
						      	'title' => 'Nội dung',
						    )
					  	),
					),
					array(
					  	'id'        => 'opt-group-quote',
					  	'type'      => 'group',
					  	'title'     => 'Văn bản trích dẫn',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-text',
					      		'type'  => 'textarea',
					      		'title' => 'Nội dung',
					    	),
						    array(
						      	'id'    => 'opt-source',
						      	'type'  => 'text',
						      	'title' => 'Nguồn nội dung',
						    )
					  	),
					),
					array(
					  	'id'        => 'opt-group-button',
					  	'type'      => 'group',
					  	'title'     => 'Button',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-icon',
					      		'type'  => 'media',
					      		'title' => 'Icon',
					    	),
						    array(
						      	'id'    => 'opt-text',
						      	'type'  => 'text',
						      	'title' => 'Tiêu đề',
						    ),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết',
						    )
					  	),
					),
			  	),
			),
    	)
  	) );

	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page IOT',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-iot-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-iot-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-iot-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),

			// Section USE YOUR DATA WISELY WITH IIOT
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: USE YOUR DATA WISELY WITH IIOT',
			),
			array(
	        	'id'    => 'opt-iot-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-iot-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section UPGRADE YOUR DATA USING INDUSTRIAL IOT (IIOT)
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: UPGRADE YOUR DATA USING INDUSTRIAL IOT (IIOT)',
			),
			array(
	        	'id'    => 'opt-iot-section3-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-iot-section3-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),
	     	array(
			  	'id'     => 'opt-home-section3-fieldset1',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 1',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-home-section3-fieldset2',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 2',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-home-section3-fieldset3',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 3',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-home-section3-fieldset4',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 4',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-home-section3-fieldset5',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 5',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
	    )
	));

		// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page Innovation Service',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-is-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-is-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-is-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),

			// Section INSIGHTFUL, DEVELOPER-READY CAMPUS AREA NETWORK
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: INSIGHTFUL, DEVELOPER-READY CAMPUS AREA NETWORK',
			),
			array(
	        	'id'    => 'opt-is-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-is-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section Image
	     	array(
	        	'id'    => 'opt-is-left-image',
	        	'type'  => 'media',
	        	'title' => 'Hình ảnh khung trái',
	     	),
	     	array(
	        	'id'    => 'opt-is-right-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-is-right-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),
	     	array(
			  	'id'        => 'opt-is-link',
			  	'type'      => 'fieldset',
			  	'title'     => 'Liên kết',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),
	    )
	));


	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page Networking',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-networking-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-networking-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-networking-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),

			// Section INSIGHTFUL, DEVELOPER-READY CAMPUS AREA NETWORK
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: INSIGHTFUL, DEVELOPER-READY CAMPUS AREA NETWORK',
			),
			array(
	        	'id'    => 'opt-networking-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-networking-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section Image
	     	array(
	        	'id'    => 'opt-networking-left-image',
	        	'type'  => 'media',
	        	'title' => 'Hình ảnh khung trái',
	     	),
	     	array(
	        	'id'    => 'opt-networking-right-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-networking-right-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),
	     	array(
			  	'id'        => 'opt-group-link',
			  	'type'      => 'fieldset',
			  	'title'     => 'Liên kết',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),
	    )
	));

	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page Cloud',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-cloud-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-cloud-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-cloud-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),

			// Section MEET YOUR TOUGHEST CLOUD CHALLENGES HEAD ON WITH HPE
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: MEET YOUR TOUGHEST CLOUD CHALLENGES HEAD ON WITH HPE',
			),
			array(
	        	'id'    => 'opt-cloud-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-cloud-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section Group
	   		array(
			  	'id'        => 'opt-cloud-item-group',
			  	'type'      => 'group',
			  	'title'     => 'Group Item',
			  	'fields'    => array(
			    	array(
					  	'id'     => 'opt-cloud-section3-item',
					  	'type'   => 'fieldset',
					  	'title'  => 'Item',
					  	'fields' => array(
						    array(
						      	'id'    => 'opt-image',
						      	'type'  => 'media',
						      	'title' => 'Hình ảnh',
						    ),
						    array(
						      	'id'    => 'opt-title',
						      	'type'  => 'text',
						      	'title' => 'Tiêu đề',
						    ),
						    array(
						      	'id'    => 'opt-des',
						      	'type'  => 'textarea',
						      	'title' => 'Mô tả',
						    ),
					  	),
					),
			  	),
			),

			array(
			  	'id'        => 'opt-cloud-button-link',
			  	'type'      => 'fieldset',
			  	'title'     => 'Button cuối trang',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),
	    )
	));

	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page Transformation Service',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-ts-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-ts-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-ts-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),
	     	array(
			  	'id'        => 'opt-ts-button-1',
			  	'type'      => 'fieldset',
			  	'title'     => 'Button 1',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),
			array(
			  	'id'        => 'opt-ts-button-2',
			  	'type'      => 'fieldset',
			  	'title'     => 'Button 2',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),

			// Section DIGITAL TRANSFORMATION IS THE NEW IT GOAL
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: DIGITAL TRANSFORMATION IS THE NEW IT GOAL',
			),
			array(
	        	'id'    => 'opt-ts-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-ts-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section CHART YOUR DIGITAL TRANSFORMATION PATH FROM START TO OUTCOME
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: CHART YOUR DIGITAL TRANSFORMATION PATH FROM START TO OUTCOME',
			),
			array(
	        	'id'    => 'opt-ts-section3-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-ts-section3-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	array(
			  	'id'     => 'opt-ts-section3-fieldset1',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 1',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-ts-section3-fieldset2',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 2',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-ts-section3-fieldset3',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 3',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-ts-section3-fieldset4',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 4',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
	    )
	));

	// Create a section
  	CSF::createSection( $prefix, array(
    	'title'  => 'Page Compute',
    	'fields' => array(
    		// Section banner
    		array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: BANNER',
			),
    		array(
	        	'id'    => 'opt-compute-banner-image',
	        	'type'  => 'media',
	        	'title' => 'Banner - Ảnh Banner',
	     	),
	     	array(
	        	'id'    => 'opt-compute-banner-title',
	        	'type'  => 'text',
	        	'title' => 'Banner - Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-compute-banner-des',
	        	'type'  => 'textarea',
	        	'title' => 'Banner - Mô tả',
	     	),
	     	array(
			  	'id'        => 'opt-compute-button-1',
			  	'type'      => 'fieldset',
			  	'title'     => 'Button 1',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),
			array(
			  	'id'        => 'opt-compute-button-2',
			  	'type'      => 'fieldset',
			  	'title'     => 'Button 2',
			  	'fields'    => array(
			    	array(
			      		'id'    => 'opt-title',
			      		'type'  => 'text',
			      		'title' => 'Tiêu đề liên kết',
			    	),
				    array(
				      	'id'    => 'opt-link',
				      	'type'  => 'text',
				      	'title' => 'Liên kết ngoài',
				    )
			  	),
			),

			// Section DIGITAL TRANSFORMATION IS THE NEW IT GOAL
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: DIGITAL TRANSFORMATION IS THE NEW IT GOAL',
			),
			array(
	        	'id'    => 'opt-compute-section2-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-compute-section2-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	// Section CHART YOUR DIGITAL TRANSFORMATION PATH FROM START TO OUTCOME
			array(
			  	'type'    => 'heading',
			  	'content' => 'SECTION: CHART YOUR DIGITAL TRANSFORMATION PATH FROM START TO OUTCOME',
			),
			array(
	        	'id'    => 'opt-compute-section3-title',
	        	'type'  => 'text',
	        	'title' => 'Tiêu đề lớn',
	     	),
	     	array(
	        	'id'    => 'opt-compute-section3-des',
	        	'type'  => 'textarea',
	        	'title' => 'Mô tả',
	     	),

	     	array(
			  	'id'     => 'opt-compute-section3-fieldset1',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 1',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-compute-section3-fieldset2',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 2',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-compute-section3-fieldset3',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 3',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-image',
				      	'type'  => 'media',
				      	'title' => 'Hình ảnh',
				    ),
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề lớn dưới ảnh',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả ngắn dưới ảnh',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-compute-section3-fieldset4',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 4',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
			array(
			  	'id'     => 'opt-compute-section3-fieldset5',
			  	'type'   => 'fieldset',
			  	'title'  => 'Item 5',
			  	'fields' => array(
				    array(
				      	'id'    => 'opt-title',
				      	'type'  => 'text',
				      	'title' => 'Tiêu đề',
				    ),
				    array(
				      	'id'    => 'opt-des',
				      	'type'  => 'textarea',
				      	'title' => 'Mô tả',
				    ),
				    array(
					  	'id'        => 'opt-group-link',
					  	'type'      => 'fieldset',
					  	'title'     => 'Liên kết',
					  	'fields'    => array(
					    	array(
					      		'id'    => 'opt-title',
					      		'type'  => 'text',
					      		'title' => 'Tiêu đề liên kết',
					    	),
						    array(
						      	'id'    => 'opt-link',
						      	'type'  => 'text',
						      	'title' => 'Liên kết ngoài',
						    )
					  	),
					),
			  	),
			),
	    )
	));
}

if ( ! function_exists( '_get_option' ) ) {
  	function _get_option( $option = '', $default = null ) {
    	$options = get_option( 'devng_option' );

    	return ( isset( $options[$option] ) ) ? $options[$option] : $default;
  	}
}