<?php
/**
* Template Name: HPE - Innovation Services
*
* @package WordPress
* @subpackage HPE
* @since HPE 1.0
*/

get_header();
?>

	<main class="page-content " id="content" role="main" tabindex="-1">
	    <div class="polaris-body-zone parsys">
	        <div class="simpleMarqueeComponent parbase">
	            <div class="simple-marquee typo3 " data-analytics-region-id="simplemrq_1">
	                <div class="marquee-content hf-centered-content">
	                    <div class="content-wrapper">
	                        <div class="content-area-wrapper">
	                            <div class="content-area focusable" style="width: 50%">
	                                <h1 class="title " style="color: #000; float: left; font-family: 'Metric Semibold',Arial,sans-serif;"><?php echo _get_option('opt-is-banner-title', 'CAMPUS AREA NETWORK AND BRANCH NETWORKING'); ?></h1>
	                                <div class="desc body-copy-large rich-text-container " style="color: #000; float: left;"><?php echo _get_option('opt-is-banner-des'); ?></div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="background-wrapper ">
	                    <div class="img-container img-container-loaded">
	                        <img src="<?php echo _get_option('opt-is-banner-image')['url']; ?>" alt="">
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="introductionComponent parbase">
	            <style type="text/css" data-href="/apps/hpeweb-ui/css/components/design-3.0/introduction.c192970b.css"> /*|1.73.1.35-S-35|publish|2020-11-02T16:00:36.033Z|*/.introduction .text-collapsible-wrapper p:last-child,.introduction.large-mode .text-collapsible-wrapper-large-mode p:last-child,.text-collapsible-wrapper .text-collapsible h2:last-child,.text-collapsible-wrapper .text-collapsible h3:last-child,.text-collapsible-wrapper .text-collapsible h4:last-child,.text-collapsible-wrapper .text-collapsible h5:last-child{margin-bottom:0}.text-collapsible-wrapper{display:flex;align-items:flex-start}.text-collapsible-wrapper .text-collapsible{flex:1 1 auto;overflow:hidden}.text-collapsible-wrapper .text-collapsible h2:first-child,.text-collapsible-wrapper .text-collapsible h3:first-child,.text-collapsible-wrapper .text-collapsible h4:first-child,.text-collapsible-wrapper .text-collapsible h5:first-child{margin-top:0}.text-collapsible-wrapper .tc-trigger{display:none;flex:0 0 auto;margin-left:5px;align-self:flex-end;font-family:'Metric Semibold';color:#01A982}.text-collapsible-wrapper.tc-enabled .tc-trigger{display:block}.text-collapsible-wrapper.tc-enabled .expanded-label{display:none}.text-collapsible-wrapper.tc-enabled .collapsed-label,.text-collapsible-wrapper.tc-enabled.tc-expanded .expanded-label{display:inline}.introduction.large-mode .text-collapsible-wrapper-large-mode .collapsed-label,.text-collapsible-wrapper.tc-enabled.tc-expanded .collapsed-label{display:none}.introduction{padding:49px 0 58px}@media (min-width:1200px) and (max-width:1599px){.introduction.h1-active:not(.large-mode){padding-top:47px}}@media (min-width:992px) and (max-width:1199px){.introduction.h1-active:not(.large-mode){padding-top:50px}}@media (max-width:991px){.introduction.h1-active:not(.large-mode){padding-top:51px}}@media (min-width:1200px) and (max-width:1599px){.introduction.h2-active{padding-top:50px}}@media (min-width:768px) and (max-width:1199px){.introduction.h2-active{padding-top:51px}}@media (max-width:767px){.text-collapsible-wrapper{flex-direction:column}.text-collapsible-wrapper .text-collapsible{flex:1 0 auto}.text-collapsible-wrapper .tc-trigger{align-self:center}.introduction.h2-active{padding-top:52px}}.introduction.h3-active{padding-top:52px}@media (max-width:991px){.introduction.h3-active{padding-top:53px}}.introduction.h4-active,.introduction.h5-active{padding-top:52px}@media (max-width:1199px){.introduction.h4-active,.introduction.h5-active{padding-top:53px}}.introduction.large-mode{padding-top:41px;padding-bottom:60px}@media (min-width:1200px) and (max-width:1599px){.introduction.large-mode{padding-top:47px;padding-bottom:40px}}@media (min-width:768px) and (max-width:1199px){.introduction.large-mode{padding-top:50px;padding-bottom:40px}}@media (max-width:991px){.introduction.large-mode{padding-bottom:30px}}.introduction.hr-enabled{padding-bottom:0}.introduction .introduction-header{padding-bottom:0;margin-top:0;margin-bottom:0}.introduction .introduction-header.dashed-text:after{bottom:0}.introduction h1:not(.large-header).dashed-text{padding-bottom:28px}.introduction h1:not(.large-header)+.text-collapsible-wrapper{margin-top:20px}.introduction h1:not(.large-header):not(.dashed-text)+.text-collapsible-wrapper{margin-top:13px}@media (min-width:1200px) and (max-width:1599px){.introduction h1:not(.large-header).dashed-text{padding-bottom:18px}.introduction h1:not(.large-header)+.text-collapsible-wrapper{margin-top:13px}.introduction h1:not(.large-header):not(.dashed-text)+.text-collapsible-wrapper{margin-top:15px}}@media (max-width:1199px){.introduction h1:not(.large-header).dashed-text{padding-bottom:20px}.introduction h1:not(.large-header)+.text-collapsible-wrapper{margin-top:14px}.introduction h1:not(.large-header):not(.dashed-text)+.text-collapsible-wrapper{margin-top:17px}}.introduction h2.dashed-text{padding-bottom:28px}.introduction h2+.text-collapsible-wrapper{margin-top:23px}.introduction h2:not(.dashed-text)+.text-collapsible-wrapper{margin-top:16px}@media (min-width:1200px) and (max-width:1599px){.introduction h2.dashed-text{padding-bottom:19px}.introduction h2+.text-collapsible-wrapper{margin-top:13px}.introduction h2:not(.dashed-text)+.text-collapsible-wrapper{margin-top:16px}}@media (max-width:1199px){.introduction h2.dashed-text{padding-bottom:21px}.introduction h2+.text-collapsible-wrapper{margin-top:13px}.introduction h2:not(.dashed-text)+.text-collapsible-wrapper{margin-top:20px}}.introduction h3.dashed-text,.introduction h4.dashed-text,.introduction h5.dashed-text{padding-bottom:30px}.introduction h3+.text-collapsible-wrapper,.introduction h4+.text-collapsible-wrapper,.introduction h5+.text-collapsible-wrapper{margin-top:23px}.introduction h3:not(.dashed-text)+.text-collapsible-wrapper,.introduction h4:not(.dashed-text)+.text-collapsible-wrapper,.introduction h5:not(.dashed-text)+.text-collapsible-wrapper{margin-top:15px}@media (max-width:1599px){.introduction h3.dashed-text,.introduction h4.dashed-text,.introduction h5.dashed-text{padding-bottom:20px}.introduction h3+.text-collapsible-wrapper,.introduction h4+.text-collapsible-wrapper,.introduction h5+.text-collapsible-wrapper{margin-top:13px}.introduction h3:not(.dashed-text)+.text-collapsible-wrapper,.introduction h4:not(.dashed-text)+.text-collapsible-wrapper,.introduction h5:not(.dashed-text)+.text-collapsible-wrapper{margin-top:18px}}@media (max-width:991px){.introduction h3+.text-collapsible-wrapper,.introduction h4+.text-collapsible-wrapper,.introduction h5+.text-collapsible-wrapper{margin-top:15px}.introduction h3:not(.dashed-text)+.text-collapsible-wrapper,.introduction h4:not(.dashed-text)+.text-collapsible-wrapper,.introduction h5:not(.dashed-text)+.text-collapsible-wrapper{margin-top:20px}}.introduction .large-header{padding-bottom:0;font-size:80px;line-height:1em}.introduction .large-header.dashed-text{padding-bottom:32px}.introduction .large-header:not(.dashed-text)+.text-collapsible-wrapper-large-mode{margin-top:15px}@media (min-width:1200px) and (max-width:1599px){.introduction .large-header{font-size:53px;line-height:1em}.introduction .large-header.dashed-text{padding-bottom:37px}.introduction .large-header:not(.dashed-text)+.text-collapsible-wrapper-large-mode{margin-top:25px}}@media (min-width:992px) and (max-width:1199px){.introduction .large-header{font-size:42px;line-height:1em}.introduction .large-header.dashed-text{padding-bottom:30px}.introduction .large-header:not(.dashed-text)+.text-collapsible-wrapper-large-mode{margin-top:18px}}@media (min-width:768px) and (max-width:991px){.introduction .large-header{font-size:34px;line-height:1em}.introduction .large-header.dashed-text{padding-bottom:20px}.introduction .large-header:not(.dashed-text)+.text-collapsible-wrapper-large-mode{margin-top:10px}}@media (max-width:767px){.introduction .large-header{font-size:28px;line-height:1em}.introduction .large-header.dashed-text{padding-bottom:20px}.introduction .large-header:not(.dashed-text)+.text-collapsible-wrapper-large-mode{margin-top:10px}}.introduction .large-header+.text-collapsible-wrapper-large-mode{margin-top:35px}@media (min-width:1200px) and (max-width:1599px){.introduction .large-header+.text-collapsible-wrapper-large-mode{margin-top:32px}}@media (min-width:992px) and (max-width:1199px){.introduction .large-header+.text-collapsible-wrapper-large-mode{margin-top:25px}}@media (max-width:991px){.introduction .large-header+.text-collapsible-wrapper-large-mode{margin-top:15px}}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible{font-size:30px;line-height:1.13333333em}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible i{font-size:46px;line-height:1em}@media (min-width:1200px) and (max-width:1599px){.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible{font-size:22px;line-height:1.09090909em}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible i{font-size:31px;line-height:1em}}@media (min-width:992px) and (max-width:1199px){.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible{font-size:20px;line-height:1.1em}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible i{font-size:24px;line-height:1em}}@media (min-width:768px) and (max-width:991px){.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible{font-size:18px;line-height:1.11111111em}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible i{font-size:20px;line-height:1em}}@media (max-width:767px){.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible{font-size:16px;line-height:1.125em}.introduction.large-mode .text-collapsible-wrapper-large-mode .text-collapsible i{font-size:17px;line-height:1em}}.introduction.large-mode .horizontal-ruler{margin-top:58px}@media (min-width:992px) and (max-width:1599px){.introduction.large-mode .horizontal-ruler{margin-top:38px}}@media (max-width:991px){.introduction.large-mode .horizontal-ruler{margin-top:28px}}.introduction .text-collapsible-wrapper{display:block}.introduction .text-collapsible-wrapper.tc-enabled.tc-expanded{display:none}.introduction .text-collapsible-wrapper.tc-enabled .collapsed-label{display:inline-block}.introduction .text-collapsible-wrapper .tc-trigger{cursor:pointer;text-transform:lowercase;margin-bottom:-.28em;margin-top:calc(25px - .27999999999999997em)}.introduction .horizontal-ruler{margin-top:58px;padding:0}.columns-component.style-bg-bronze .introduction .collapsed-label,.columns-component.style-bg-bronze .introduction .introduction-header,.columns-component.style-bg-bronze .introduction .rich-text-container,.columns-component.style-bg-dark-steel .introduction .collapsed-label,.columns-component.style-bg-dark-steel .introduction .introduction-header,.columns-component.style-bg-dark-steel .introduction .rich-text-container,.columns-component.style-bg-gray-5 .introduction .introduction-header,.columns-component.style-bg-gray-5 .introduction .rich-text-container,.columns-component.style-bg-purple .introduction .collapsed-label,.columns-component.style-bg-purple .introduction .introduction-header,.columns-component.style-bg-purple .introduction .rich-text-container,.columns-component.style-bg-slate .introduction .collapsed-label,.columns-component.style-bg-slate .introduction .introduction-header,.columns-component.style-bg-slate .introduction .rich-text-container{color:#FFF} </style>
	            <style>.introduction.introduction-1477786212:not(#add-spec){padding-top:54px;padding-bottom:0px}@media (min-width: 768px) {.introduction.introduction-1477786212:not(#add-spec){padding-top:51px;padding-bottom:0px}}@media (min-width: 992px) {.introduction.introduction-1477786212:not(#add-spec){padding-top:51px;padding-bottom:0px}}@media (min-width: 1200px) {.introduction.introduction-1477786212:not(#add-spec){padding-top:51px;padding-bottom:0px}}@media (min-width: 1600px) {.introduction.introduction-1477786212:not(#add-spec){padding-top:49px;padding-bottom:0px}}</style>
	            <div class="introduction introduction-1477786212 coloredtheme-aruba-orange h2-active typo3 hr-enabled text-center white " data-analytics-region-id="introcomp_1">
	                <div class="centered-content">
	                    <h2 class="introduction-header dashed-text text-center "> <?php echo _get_option('opt-is-section2-title'); ?> </h2>
	                    <div class="text-collapsible-wrapper" core-view="">
	                        <div class="body-copy rich-text-container text-collapsible " style="max-height: 104px;">
	                            <p><?php echo _get_option('opt-is-section2-des'); ?></p>
	                        </div>
	                        <h5 class="tc-trigger collapsed-label" aria-label="show more">+ show more</h5>
	                    </div>
	                    <hr class="horizontal-ruler">
	                </div>
	            </div>
	        </div>
	        <div class="columnContainerComponent parbase">
	            <div class="columns-component column-content-1 none typo3 " style="padding-left: 0px; padding-right: 0px; padding-bottom: 0px;padding-top: 0px;margin-left: 0px; margin-right: 0px; margin-bottom: 0px; margin-top: 0px;">
	                <div class="container-gutter">
	                    <div class="row">
	                        <div class="columns-component-col ">
	                            <div class="contentBlock parbase">
	                                <link rel="stylesheet" href="/apps/hpeweb-ui/css/components/hpe-2.0/content-block.02587d26.css">
	                                <style>.hpe-content-block-1212788373{padding-top:30px;padding-bottom:30px}@media (min-width: 768px) {.hpe-content-block-1212788373{padding-top:60px;padding-bottom:60px}}@media (min-width: 992px) {.hpe-content-block-1212788373{padding-top:60px;padding-bottom:60px}}@media (min-width: 1200px) {.hpe-content-block-1212788373{padding-top:60px;padding-bottom:60px}}@media (min-width: 1600px) {.hpe-content-block-1212788373{padding-top:60px;padding-bottom:60px}}</style>
	                                <div class="hpe-content-block hpe-content-block-1212788373 cb-50-50-left typo3 coloredtheme-specific-gray " data-analytics-region-id="contentblock_1" core-view="">
	                                    <div class="cb-media " style="max-width: 48%; float: left; padding-right: 1%;">
	                                        <div class="item-media ">
	                                            <img src="<?php echo _get_option('opt-is-left-image')['url']; ?>" alt="">
	                                        </div>
	                                    </div>
	                                    <div class="cb-content" style="max-width: 48%; float: left; padding-left: 1%;">
	                                        <a class="cb-title tag-h3 " href="https://www.arubanetworks.com/products/networking/arubaos/" x-cq-linkchecker="skip" data-analytics-region-id="contentblock_1|header" target="_blank"><?php echo _get_option('opt-is-right-title'); ?></a> 
	                                        <div class="cb-desc body-copy rich-text-container ">
	                                            <p><?php echo _get_option('opt-is-right-des'); ?></p>
	                                        </div>
	                                        <ul class="cb-links">
	                                            <li>
	                                                <a class="secondary-link primary button-cta " href="<?php echo _get_option('opt-group-link')['opt-link']; ?>" target="_blank">
	                                                    <em class="icon-arrow-link-right"></em> <?php echo _get_option('opt-group-link')['opt-title']; ?>
	                                                    <span class="arrow-wrapper">
	                                                        <div class="arrow"></div>
	                                                    </span>
	                                                </a>
	                                            </li>
	                                        </ul>
	                                    </div>
	                                </div>
	                                <!-- END: Content Block component --> 
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="cta-container" style="margin: 50px 0;">
			    <style>.cta-804421618{margin-top:0px;margin-bottom:55px;margin-left:0px;margin-right:0px}</style>
			    <a class="cta typo3 btn-v2 light-bkg pull-center cta-804421618 " href="<?php echo _get_option('opt-is-link')['opt-link']; ?>"> <?php echo _get_option('opt-is-link')['opt-title']; ?> </a> 
			</div>
	    </div>
	</main>

<?php 
get_footer();
?>