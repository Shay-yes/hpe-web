<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HPE
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

    <link rel="icon" type="image/vnd.microsoft.icon" href="https://www.hpe.com/etc/designs/hpeweb/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="https://www.hpe.com/etc/designs/hpeweb/favicon.ico">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/apps/hpeweb-ui/css/hpe-generic-v3.f51d42df.css"/>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/dcustom.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/cus-style.css"/>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'hpe' ); ?></a>
    <header class="hpe-header arrow-type hf-v3">
        <div class="newpar new section"> </div>
        <div class="par iparys_inherited">
            <div class="globalNavigationWrapper parbase">
                <div>
                    <div id="hf-header-v3" class="dark" data-is-us-page="true">
                       <!--  <nav class="hf-utility-nav">
                            <div class="hf-centered-content">
                            </div>
                        </nav> -->
                        <nav class="hf-nav left-align vertical-line">
                            <!-- <PRIMARY-SECTION> --> 
                            <div class="hf-centered-content">
                                <div class="hf-logo-menu enabled">
                                    <div class="hf-logo" data-target-id="GN_v3_logoMenuFlyout">
                                        <a class="hf-logo-btn" title="Hewlett Packard Enterprise" aria-label="Hewlett Packard Enterprise" href="<?php echo get_home_url(); ?>">
                                            <?xml version="1.0" encoding="utf-8" standalone="no"?> <!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0) -->
                                            <img src="/wp-content/themes/hpe/assets/images/logo-2.png">
                                        </a>
                                       
                                    </div>
                                </div>
                                <div class="hf-menu-dynamic">
                                    <ul class="hf-menu" role="menubar">
                                        <li class="hf-menu-item disable-flyout" role="menuitem"> <a class="hf-nav-link " href="javascript:;"> <span class="hf-link-text ">Solution</span> </a> <span class="hf-flyout-arrow"></span>
                                            <ul class="hf-sub-menu">
                                                <li class="hf-menu-item disable-flyout" role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/compute"> <span class="hf-link-text ">Computer</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                                <li class="hf-menu-item disable-flyout" role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/iot"> <span class="hf-link-text ">IOT</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                                <li class="hf-menu-item disable-flyout" role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/innovation-service"> <span class="hf-link-text ">Innovation Service</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                                <li class="hf-menu-item disable-flyout" role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/networking"> <span class="hf-link-text ">Networking</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                                <li class="hf-menu-item " role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/cloud"> <span class="hf-link-text ">Hybrid cloud solution</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                                <li class="hf-menu-item " role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/transformation-service"> <span class="hf-link-text ">Transformation service</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                            </ul>
                                        </li>
                                        <li class="hf-menu-item " role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/news"> <span class="hf-link-text ">News</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                        <li class="hf-menu-item " role="menuitem"> <a class="hf-nav-link " href="<?php echo get_home_url(); ?>/contact"> <span class="hf-link-text ">Contact Us</span> </a> <span class="hf-flyout-arrow"></span> </li>
                                        
                                       
                                    </ul>

                                </div>
                                <div class="hf-menu-fixed">
                                    <ul class="hf-menu" role="menubar">
                                        <li class="hf-menu-item hf-hmb-item" role="menuitem" id="hf-hmb-item-v3">
                                            <a class="hf-nav-link" href="javascript: void 0;" data-analytics-region-id="gmenu|Menu Icon" onclick="void 0" tabindex="0" role="button">
                                                <div class="hf-hmb-icon">
                                                    <div class="hf-line"></div>
                                                    <div class="hf-line"></div>
                                                    <div class="hf-line"></div>
                                                </div>
                                                <span class="hf-link-text hf-alt-text hf-hmb-text-menu">Menu</span> <span class="hf-link-text hf-alt-text hf-hmb-text-close">Close</span> 
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- </PRIMARY-SECTION> --> <!-- <SEARCH-BOX> --> 
                            <div id="hf-search-container-v3" tabindex="-1">
                                <div id="hf-search-box-v3">
                                    <div class="hf-centered-content">
                                        <form id="hf-search-form-v3" action="https://www.hpe.com/us/en/search-results.html?page=1&amp;q=" class="js-search-box" data-search-properties="{&#34;/newsroom/&#34;:&#34;more:newsroom&#34;,&#34;/newsroom.html&#34;:&#34;more:newsroom&#34;}" data-autocomplete="header" data-autocomplete-lang="&lang=en" data-autocomplete-url="https://hpe.c.lucidworks.cloud/api/apps/hpe/query/suggest?rows=5&amp;suggest.q=">
                                            <label for="hf-search-input-v3" class="sr-only">Search</label> <input id="hf-search-input-v3" type="text" class="js-search-input" placeholder="Search hpe.com" autocomplete="off" spellcheck="false" tabindex="0" name="s-query"/> 
                                            <button type="submit" class="hf-search-submit" tabindex="0" title="Search" aria-label="Search">
                                                <svg class="hf-icon-search" focusable="false" viewBox="0 0 24 24" role="presentation" aria-hidden="true">
                                                    <path d="M22.3,23.9l-7.2-7.2c-1.6,1.3-3.6,2-5.8,2c-5.1,0-9.3-4.2-9.3-9.3c0-5.1,4.2-9.3,9.3-9.3c5.1,0,9.3,4.2,9.3,9.3 c0,2.2-0.7,4.2-2,5.8l7.2,7.2L22.3,23.9z M9.4,2.3c-3.9,0-7.1,3.2-7.1,7.1s3.2,7.1,7.1,7.1s7.1-3.2,7.1-7.1S13.3,2.3,9.4,2.3z"></path>
                                                </svg>
                                            </button>
                                        </form>
                                        <button class="hf-search-close" tabindex="0" title="Close" aria-label="Close" data-analytics-region-id="gmenu|close" onclick="void 0">
                                            <svg class="hf-icon hf-icon-close" focusable="false" viewBox="0 0 24 24" role="presentation" aria-hidden="true">
                                                <path d="M 0,0 L 24,24 M 0,24 L 24,0" stroke="#01A982" stroke-width="3"></path>
                                            </svg>
                                            <span class="hf-alt-text">Close</span> 
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- </SEARCH-BOX> --> 
                        </nav>
                        <div class="hf-flyout-container-v3">
                            <div class="hf-centered-content">
                                <div class="hf-flyout-area-v3">
                                    <div id="globalnavflyoutcompo_189610330" class="hf-flyout-v3" role="region" aria-label="Header flyout menu">
                                    </div>
                                    <div class="hf-flyout-v3 hf-flyout-login" id="hf-hpe-login">
                                    </div>
                                    <div class="hf-flyout-v3 hf-flyout-logout" id="hf-hpe-logout">
                                    </div>
                                    <div id="GN_v3_ContactUsFlyout" class="hf-flyout-v3 hf-flyout-contact-v3" role="region" aria-label="Header flyout menu">
                                    </div>
                                    <div class="hf-flyout-v3 hf-flyout-all-hpe" id="GN_v3_allHPEFlyout">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav class="hf-mobile-nav" id="hf-mobile-nav-v3">
                            <div class="hf-menu-mobile">
                                <div class="hf-centered-content">
                                     <ul class="hf-mobile-menu">
                                        <li class="hf-item ">
                                              <span class="hf-mobile-menu-item hf-mob-diff" href="javascript: void 0;"> Solution </span>
                                             <div class="hf-mobile-submenu">
                                                <a class="hf-mobile-submenu-item  " href=""> Computer </a> 
                                                <a class="hf-mobile-submenu-item  " href=""> IOT </a> 
                                                <a class="hf-mobile-submenu-item  " href=""> Innovation Service </a> 
                                                <a class="hf-mobile-submenu-item  " href=""> Networking </a> 
                                                <a class="hf-mobile-submenu-item " href=""> Hybrid cloud solution </a>
                                                <a class="hf-mobile-submenu-item" href="">Transformation service </a>  
                                             </div>
                                        </li>
                                        <li class="hf-item ">
                                            <a class="hf-mobile-menu-item hf-mob-next" href="javascript: void 0;"> News </a>
                                        </li>
                                        <li class="hf-item ">
                                            <a class="hf-mobile-menu-item hf-mob-next" href="javascript:void 0;"> Contact </a>
                                        </li>
                                        
                                    </ul> 
                                    <div class="utility-items-container"> </div>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="hf-search-overlay"></div>
                    <script id="cockpit-loader" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/apps/hpeweb-ui/js/hpe.cockpit-loader.js" data-content="/us/en/cockpit/cockpit.html" data-btn-label="Cockpit" data-close-label="Close" defer></script> 
                </div>
            </div>
            <div class="ghost"> </div>
        </div>
        <div class="newpar new section"> </div>
        <div class="par iparys_inherited"> </div>
    </header>



