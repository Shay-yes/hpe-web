/*|1.73.1.35-S-35|publish|2020-11-02T16:00:36.033Z|*/ ! function(e) {
    e.version = e.version || "1.73.1.35-S-35", e.time = e.time || new Date(1604332836033)
}(window["~hpe~"] || (window["~hpe~"] = {})),
function(n) {
    var o = {},
        e = {
            "cockpit/api/cockpit-events": 271,
            "cockpit/core/core": 280,
            "cockpit/helpers/loaders": 436,
            "cockpit/cockpit-loader": 655
        },
        t = "core/core";

    function i(e) {
        if (o[e]) return o[e].exports;
        var t = o[e] = {
            i: e,
            l: !1,
            exports: {}
        };
        return n[e].call(t.exports, t, t.exports, i), t.l = !0, t.exports
    }
    void 0 !== e[t] && i(e[t]).registerWebpackRequire(i, e), i.m = n, i.c = o, i.d = function(e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: n
        })
    }, i.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, i.t = function(t, e) {
        if (1 & e && (t = i(t)), 8 & e) return t;
        if (4 & e && "object" == typeof t && t && t.__esModule) return t;
        var n = Object.create(null);
        if (i.r(n), Object.defineProperty(n, "default", {
                enumerable: !0,
                value: t
            }), 2 & e && "string" != typeof t)
            for (var o in t) i.d(n, o, function(e) {
                return t[e]
            }.bind(null, o));
        return n
    }, i.n = function(e) {
        var t = e && e.__esModule ? function() {
            return e.default
        } : function() {
            return e
        };
        return i.d(t, "a", t), t
    }, i.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, i.p = "", i(i.s = 654)
}({
    271: function(e, t, n) {
        "use strict";
        n.r(t), n.d(t, "AUTH_LOGIN_EVENT", function() {
            return o
        }), n.d(t, "AUTH_LOGOUT_EVENT", function() {
            return i
        }), n.d(t, "AUTH_LOGGED_EVENT", function() {
            return r
        }), n.d(t, "COCKPIT_SIDEBAR_LOADED", function() {
            return d
        }), n.d(t, "COCKPIT_CORE_READY_EVENT", function() {
            return c
        }), n.d(t, "CockpitEvents", function() {
            return u
        });
        var o = "auth:api:login",
            i = "auth:api:logout",
            r = "auth:api:logged",
            d = "cockpit:sidebar:loaded",
            c = "cockpit:core:ready",
            u = (a.loginHappened = function(e) {
                void 0 === e && (e = {}), this.userLogged || this.fireEvent(o, e), this.userLogged = !0
            }, a.logoutHappened = function(e) {
                void 0 === e && (e = {}), this.userLogged && this.fireEvent(i, e), this.userLogged = !1
            }, a.userAlreadyLogged = function(e) {
                void 0 === e && (e = {}), this.userLogged || this.fireEvent(r, e), this.userLogged = !0
            }, a.fireEvent = function(e, t) {
                void 0 === t && (t = {}), window.dispatchEvent(new CustomEvent("" + e, {
                    detail: t
                }))
            }, a);

        function a() {}
    },
    280: function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.onDocumentReady = function(e) {
            "loading" === document.readyState ? document.addEventListener("DOMContentLoaded", e) : e()
        }, t.default = t.dispatchCoreReady = t.buildBundleName = t.defineModuleRoot = t.hpeNs = t.ns = t.COCKPIT = t.HPE_NS = void 0;
        var o = n(271),
            i = "~hpe~";
        t.HPE_NS = i;
        var r = "~cockpit~";
        t.COCKPIT = r, window[r] || (Object.defineProperty(window, r, {
            value: {
                coreReady: !1
            }
        }), Object.defineProperty(window[r], "$coreReady", {
            value: new Promise(function(e) {
                window.addEventListener(o.COCKPIT_CORE_READY_EVENT, e.bind(this, window[r]))
            })
        }), Object.defineProperty(window[r], "$userInSystem", {
            value: new Promise(function(e) {
                window.addEventListener(o.AUTH_LOGIN_EVENT, e.bind(this, window[r])), window.addEventListener(o.AUTH_LOGGED_EVENT, e.bind(this, window[r]))
            })
        }));
        var d = window[r];
        t.ns = d;
        var c = window[i];
        t.hpeNs = c, n.p = d.root || c && c.root || "";
        t.defineModuleRoot = function(e) {
            n.p = d.root = e
        };
        t.buildBundleName = function(e, t) {
            var n = (d.root || "").replace(/(js)?[/\\]$/, t);
            return "".concat(n, "/").concat(e, ".").concat(t)
        };
        t.dispatchCoreReady = function() {
            d.coreReady || setTimeout(function() {
                d.coreReady = !0, window.dispatchEvent(new CustomEvent(o.COCKPIT_CORE_READY_EVENT, {
                    detail: d
                }))
            }, 5)
        }, t.default = d, t.default.componentName = "cockpit/core/core"
    },
    436: function(e, t, n) {
        "use strict";
        n.r(t), n.d(t, "addScriptTag", function() {
            return o
        }), n.d(t, "addStyleTag", function() {
            return i
        }), n.d(t, "loadFontsIfNeeded", function() {
            return r
        });
        var o = function(e) {
                var t = document.createElement("script");
                t.src = e, t.defer = !0, document.head.appendChild(t)
            },
            i = function(e) {
                var t = document.createElement("link");
                t.href = e, t.rel = "stylesheet", document.head.appendChild(t)
            },
            r = function(e) {
                -1 === window.getComputedStyle(document.body, null).getPropertyValue("font-family").indexOf("Metric Light") && i(e)
            }
    },
    654: function(e, t, n) {
        e.exports = n(655)
    },
    655: function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.getPolyfillBundle = void 0;

        function o() {
            var e, t = window.PolyfillMode || {};
            return d[t.type] ? e = d[t.type] : "string" == typeof t.url ? e = t.url : window.Promise && window.CustomEvent && Object.assign && Array.from && window.URLSearchParams ? "attachShadow" in Element.prototype && "getRootNode" in Element.prototype && window.customElements ? "IntersectionObserver" in window && "IntersectionObserverEntry" in window || (e = d.HPE_IO) : e = d.HPE_WC : e = d.HPE_ALL, e
        }
        var i = n(280),
            r = n(436),
            d = {
                MAIN: "hpe.cockpit",
                HPE_ALL: "hpe.polyfills-all",
                HPE_WC: "hpe.polyfills-wc",
                HPE_IO: "hpe.polyfills-io"
            };
        t.getPolyfillBundle = o;
        var c = document.currentScript || document.getElementById("cockpit-loader");
        (0, i.defineModuleRoot)(c.src.replace(/hpe\.cockpit-loader\.js.*/, "")), (0, i.onDocumentReady)(function() {
            var e = o();
            e && (0, r.addScriptTag)((0, i.buildBundleName)(e, "js")), (0, r.addStyleTag)((0, i.buildBundleName)("cockpit", "css")), (0, r.loadFontsIfNeeded)((0, i.buildBundleName)("hpe.fonts.metrics", "css")), (0, r.addScriptTag)((0, i.buildBundleName)(d.MAIN, "js"))
        })
    }
})