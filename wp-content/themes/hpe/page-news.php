<?php
/**
* Template Name: HPE - News
*
* @package WordPress
* @subpackage HPE
* @since HPE 1.0
*/

get_header();
?>

    <main class="page-content news" id="content" role="main" tabindex="-1">
        <div id="content" role="main">
            <div class="hf-centered-content">
                <div class="row">
                    <?php
                    if (isset($_GET['paged']) && is_numeric($_GET['paged']))
                        $paged = (int)$_GET['paged'];
                    else
                        $paged = 1;
                    $default_posts_per_page = get_option('posts_per_page', 10);
                    ?>
                    <?php query_posts('category_name=news&posts_per_page=' . $default_posts_per_page . '&paged=' . $paged); ?>
                    <div class="col-md-9" id="primary">
                        <div class="post-list">
                            <?php while (have_posts()):the_post(); ?>
                                <div data-id="27787" class="card card-v">
                                    <div class="item">
                                        <?php $thumbnail_url = get_the_post_thumbnail_url(); ?>
                                        <?php if ($thumbnail_url): ?>

                                            <div class="card-img">
                                                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($thumbnail_url); ?>"
                                                         alt="<?php the_title(); ?>">
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="card-body">
                                            <h2 class="title">
                                                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h2>
                                            <div class="desc">
                                                <?php the_excerpt(); ?>
                                                <!-- <a class="card-readmore" title="<?php the_title(); ?>"
                                                   href="<?php the_permalink(); ?>">Đọc Tiếp</a> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                        <div class="u-cf pagination-wrap">
                            <nav class="navigation pagination" role="navigation">
                                <h2 class="screen-reader-text"></h2>
                                <div class="nav-links">
                                    <?php
                                    the_posts_pagination(
                                        [
                                            'mid_size'  => 3,
                                            'prev_text' => __('<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="angle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-left fa-w-8 fa-3x"><g class="fa-group"><path fill="currentColor" d="M223.83 352.44a24 24 0 0 1 0 33.86L201.32 409l-.18.18a23.76 23.76 0 0 1-33.6-.18l-96.15-96.5 56.47-56.5z" class="fa-secondary"></path><path fill="currentColor" d="M167.81 102.87l-.17.18L32.11 239a24 24 0 0 0-.17 33.93l.06.07 39.39 39.51L224 159.66a23.92 23.92 0 0 0 0-33.84l-22.54-22.74a23.77 23.77 0 0 0-33.62-.23z" class="fa-primary"></path></g></svg>', 'textdomain'),
                                            'next_text' => __(
                                                '<svg aria-hidden="true" focusable="false" data-prefix="fad" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512" class="svg-inline--fa fa-angle-right fa-w-8 fa-3x"><g class="fa-group"><path fill="currentColor" d="M128.14 256l56.47 56.49L88.46 409a23.76 23.76 0 0 1-33.6.18l-.18-.18-22.51-22.68a23.92 23.92 0 0 1 0-33.84z" class="fa-secondary"></path><path fill="currentColor" d="M54.58 103.07L32 125.81a23.92 23.92 0 0 0 0 33.84L184.61 312.5 224 273l.06-.06a24 24 0 0 0-.16-33.94L88.37 103l-.17-.18a23.78 23.78 0 0 0-33.62.22z" class="fa-primary"></path></g></svg>', 'textdomain'
                                            ),
                                        ]
                                    );
                                    ?>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-md-3" id="secondary">
                        <div id="sf-airheads-discussions-3" class="list-post-recent_entries widget widget_sf-airheads-discussions">
                            <?php get_sidebar(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </main>
<?php 
get_footer(); 
?>


